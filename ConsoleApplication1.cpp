﻿// ConsoleApplication1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Vector
{
public:
    Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    friend Vector operator*(const Vector& a,  int b);
    friend Vector operator-(const Vector& a, const Vector& b);
    friend std::ostream& operator<<(std::ostream& out, const Vector& v);
    friend std::istream& operator>>(std::istream& in, const Vector& v);

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "index error";
            return 0;
            break;
        }
    }

    void reconstr(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }
private:
    float x;
    float y;
    float z;
};

Vector operator*(const Vector & a,  int  b)
{
    return Vector(a.x*b,a.y*b,a.z*b);
}

Vector operator-(const Vector & a, const Vector & b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
    out << ' ' << v.x << ' ' << v.y << ' ' << v.z;
        return out;
}

std::istream& operator>>(std::istream& in,   Vector& v)
{

    
    float i ,k ,j ;
    in >> i>>k>>j;
    v.reconstr(i, k, j);
    
    
    
    return in;

    
}

int main()
{
    Vector v1(0, 1, 2);
    Vector v2(3, 5, 7);
    Vector v3(8,9,10);
    int b = 3;
    std::cout << v1 * b << '\n' ;
    std::cout << v2 - v1 << '\n';
    std::cout <<"Далее v3" << '\n';
    std::cout << v3 << '\n';
    std::cin >> v3 ;
    std::cout << v3 << '\n';
    //std::cout <<  v2[0];
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
